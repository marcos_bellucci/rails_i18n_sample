I18nXmpl::Application.routes.draw do

  resources :users

  #para aceptar el parametro ?locale=
  #a estar escrito en forma relativa
  scope "(:locale)", locale: /en|es/ do
    resources :users
  end

  root to: "home#index"


end
