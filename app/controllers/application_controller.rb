class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_locale

  #defino el valor de I18n.locale
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale	
  end

  #Obligo que todas las path otorgadas por rails 
  #tengan el parametro locale
  def default_url_options(options={})
    { locale: I18n.locale }
  end

end
