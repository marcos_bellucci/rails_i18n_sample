== README

Simple internationalization example

Application contains an empty user model.
It is build following http://guides.rubyonrails.org/i18n.html

* Ruby version 1.9.3

* rails 4

Translation/Time_format  files
   config/locales/en.yml
   config/locales/es.yml
Views
   app/views/home/index.html.erb
   app/views/user/*
Controllers
   app/controllers/home_controller.rb
          contains index method which defines
          notice message from transaltion file
   app/controllers/application_controller.rb
          defines the I18n.locale on before action
          It is defined based on locale parameter

          method default_url_options
                 ensures locale parameter is ever include
                 unless the proggrammer didn't explicity define
                 
   config/routes.rb
          to change the form example.com/?locale=es to
          example.com/es/ it was necesary to scope user resource

